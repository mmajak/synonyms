# Synonyms

This program can decide whether two words are synonyms or not.
First we have a synonym dictionary describing pairs of synonymous words. Afterwards, program answers several queries asking whether given two words are synonyms or not.
The following rules are used to decide :
1. If the pair of words is declared synonymous in the input,then they are synonyms.
2. Being synonyms doesn’t depend on order,e.g.if big is a synonym for large then large is a synonym for big.
3. We can derive the synonymous relationship indirectly:if big is a synonym for large and large is a synonym for huge then big is a synonym for huge.
4. If two words differ only by case, they are synonyms, e.g.same is a synonym for both SAmE, SAME and also same (itself).
5. If none of the above rules can be used to decide whether two words are synonyms,then they are not.

## Input file
Input starts with a number of test cases T (0 ≤ T ≤ 100). Each test case begins with a line containing a single number N (0 ≤ N ≤ 100) — the length of a synonym dictionary.
On each of the following N lines, there is exactly one pair of synonyms separated by a single space. Next line contains a single number Q (0 ≤ Q ≤ 100) — number of queries.
Each of the following lines contains a pair of query words separated by a single space.
Each word consists only of English alphabet letters ([a-zA-Z]) and is at most 20 characters long.
For each pair of query words output either string synonyms or different.
