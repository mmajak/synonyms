import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        SynonymsReader synonymsReader = new SynonymsReader();
        synonymsReader.readFile("example.in");
        List<String> expectedResults = synonymsReader.getResults();
        List<String> actualResults = new ArrayList<>();

        String fileName = "example.out";
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            for (String line; (line = br.readLine()) != null; ) {
                actualResults.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("File match : " + expectedResults.equals(actualResults));
    }
}
