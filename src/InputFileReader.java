import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class InputFileReader {

    public static void fillSynonymsDirectory(final BufferedReader reader, final Set<Set<String>> synonymsDirectory) throws IOException {
        Integer numOfSynonyms = Integer.parseInt(reader.readLine().trim());

        for (int i = 0; i < numOfSynonyms; i++) {
            String line = reader.readLine();

            String[] parts = line.split("\\s+");
            String first = parts[0];
            String second = parts[1];

            if (synonymsDirectory.isEmpty()) {
                createNewRecord(first, second, synonymsDirectory);
            } else {
                Boolean matchFound = false;

                for (Set<String> synonyms : synonymsDirectory) {
                    for (String word : synonyms) {
                        if (word.equals(first) || word.equals(second)) {
                            matchFound = true;
                        }
                    }

                    if (matchFound) {
                        synonyms.add(first.toLowerCase());
                        synonyms.add(second.toLowerCase());
                        break;
                    }
                }

                if (!matchFound) {
                    createNewRecord(first, second, synonymsDirectory);
                }
            }
        }
    }

    public static void createNewRecord(final String first, final String second, final Set<Set<String>> synonymsDirectory) {
        Set<String> newRecord = new HashSet<>();
        newRecord.add(first.toLowerCase());
        newRecord.add(second.toLowerCase());

        synonymsDirectory.add(newRecord);
    }

    public static void fillResults(final BufferedReader reader, final Set<Set<String>> synonymsDirectory, final List<String> results) throws IOException {
        Integer numOfTests = Integer.parseInt(reader.readLine().trim());

        for (int j = 0; j < numOfTests; j++) {
            String line = reader.readLine();

            String[] parts = line.split("\\s+");
            String first = parts[0];
            String second = parts[1];

            if ((first.toLowerCase().equals(second.toLowerCase())) || (areSynonyms(first.toLowerCase(), second.toLowerCase(), synonymsDirectory))) {
                results.add("synonyms");
            } else {
                results.add("different");
            }
        }
    }

    public static Boolean areSynonyms(final String first, final String second, final Set<Set<String>> synonymsDirectory) {
        Boolean areSynonyms = false;

        for (Set<String> synonyms : synonymsDirectory) {

            if (synonyms.contains(first) && synonyms.contains(second)) {
                areSynonyms = true;
                break;
            }
        }

        return areSynonyms;
    }

}
