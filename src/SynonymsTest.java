import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SynonymsTest {

    @Test
    public void whenSynonymsDirectoryIsFilled_ProvideCorrectResponses() throws IOException {
        SynonymsReader synonymsReader = new SynonymsReader();

        // when
        synonymsReader.readFile("example.in");
        List<String> synonymsReaderResults = synonymsReader.getResults();

        // then
        List<String> expectedResults = Files.lines(Paths.get("example.out")).collect(Collectors.toList());

        Assert.assertArrayEquals(expectedResults.toArray(), synonymsReaderResults.toArray());
    }

    @Test
    public void whenSynonymsBiggerDirectoryIsFilled_ProvideCorrectResponses() throws IOException {
        SynonymsReader synonymsReader = new SynonymsReader();

        // when
        synonymsReader.readFile("example_big.in");
        List<String> synonymsReaderResults = synonymsReader.getResults();

        // then
        List<String> expectedResults = Files.lines(Paths.get("example_big.out")).collect(Collectors.toList());

        Assert.assertArrayEquals(expectedResults.toArray(), synonymsReaderResults.toArray());
    }
}
