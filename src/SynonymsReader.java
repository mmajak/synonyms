import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class SynonymsReader {
    private List<String> results;

    public List<String> getResults() {
        return results;
    }

    public void readFile(final String path) {
        InputFileReader fileReader = new InputFileReader();
        Set<Set<String>> synonymsDirectory = new HashSet<>();
        results = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {

            int numOfTestCases = Integer.parseInt(br.readLine().trim());

            for (int i = 0; i < numOfTestCases; i++) {
                synonymsDirectory.clear();

                fileReader.fillSynonymsDirectory(br, synonymsDirectory);
                Set<Set<String>> joinedSynonymsDirectory = joinSynonymsFrom(synonymsDirectory);
                fileReader.fillResults(br, joinedSynonymsDirectory, results);
                System.out.println("Directory [" + i + "] : " + joinedSynonymsDirectory);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Set<Set<String>> joinSynonymsFrom(Set<Set<String>> dictionary) {
        Set<Set<String>> result = new HashSet<>();

        for (Set<String> dict : dictionary) {
            for (String syn : dict) {
                Set<String> partialSynonyms = new HashSet<>();
                for (Set<String> dd : dictionary) {
                    if (dd.contains(syn)) {
                        partialSynonyms.addAll(dd);
                    }
                }

                Optional<Set<String>> resSet = result.stream().filter(r -> !Collections.disjoint(partialSynonyms, r)).findFirst();
                if (!resSet.isPresent()) {
                    result.add(partialSynonyms);
                } else {
                    resSet.get().addAll(partialSynonyms);
                }
            }
        }
        return result;
    }

    private Set<Set<String>> joinSynonymsRecursivelyFrom(Set<Set<String>> dictionary) {
        Set<Set<String>> result = new HashSet<>();
        boolean wasChanged = false;

        for (Set<String> dict : dictionary) {
            Set<String> partialSynonyms = new HashSet<>();
            partialSynonyms.addAll(dict);
            for (String syn : dict) {
                for (Set<String> dd : dictionary) {
                    if (!dict.equals(dd)) {
                        if (dd.contains(syn)) {
                            partialSynonyms.addAll(dd);
                            wasChanged = true;
                        }
                    }
                }
            }
            result.add(partialSynonyms);
        }

        if (wasChanged) {
            return joinSynonymsRecursivelyFrom(result);
        } else {
            return result;
        }
    }

}

